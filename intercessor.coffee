module.exports =
  id: 'webgl-demos'
  title: 'WebGL Demos'
  routes: [
    ['get', '/', 'index']
    ['get', '/:demo', 'index']
  ]
